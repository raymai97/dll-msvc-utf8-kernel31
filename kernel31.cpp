#include "kernel31.h"

BOOL APIENTRY DllMain(HANDLE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
{
	switch (ul_reason_for_call) {
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

#pragma function(memset)
void * __cdecl memset(void *pTarget, int value, size_t cbTarget) {
    unsigned char *p = static_cast<unsigned char *>(pTarget);
    while (cbTarget-- > 0) {
        *p++ = static_cast<unsigned char>(value);
    }
    return pTarget;
}

KERNEL31_API
int
WINAPI
My_MultiByteToWideChar(
	UINT codepage, DWORD dwFlags, LPCSTR szMulti, int cbMulti,
	LPWSTR szWide, int cchWide)
{
	if (codepage != CP_UTF8) {
		codepage = 1252;
	}
	return MultiByteToWideChar(codepage, dwFlags, szMulti, cbMulti, szWide, cchWide);
}

KERNEL31_API
int
WINAPI
My_WideCharToMultiByte(
	UINT codepage, DWORD dwFlags, LPCWSTR szWide, int cchWide,
	LPSTR szMulti, int cbMulti, LPCSTR lpDefChar, LPBOOL lpUsedDefChar)
{
	if (codepage != CP_UTF8) {
		codepage = 1252;
	}
	return WideCharToMultiByte(codepage, dwFlags, szWide, cchWide, szMulti, cbMulti, lpDefChar, lpUsedDefChar);
}
