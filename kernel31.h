#pragma once
#include <Windows.h>

#ifndef KERNEL31_EXPORTS
#	define KERNEL31_API	extern "C" __declspec(dllimport)
#else
#	ifdef KERNEL31_64BIT
#		define KERNEL31_API	extern "C"
#	else
#		define KERNEL31_API	extern "C" __declspec(dllexport)
#	endif
#endif

extern "C" void * __cdecl memset(void *, int, size_t);
#pragma intrinsic(memset)

KERNEL31_API
int
WINAPI
My_MultiByteToWideChar(
	UINT codepage, DWORD dwFlags, LPCSTR szMulti, int cbMulti,
	LPWSTR szWide, int cchWide);

KERNEL31_API
int
WINAPI
My_WideCharToMultiByte(
	UINT codepage, DWORD dwFlags, LPCWSTR szWide, int cchWide,
	LPSTR szMulti, int cbMulti, LPCSTR lpDefChar, LPBOOL lpUsedDefChar);

#if defined MSVC2017
#	include "msvc2017.h"
#elif defined MSVC2015
#	include "msvc2015.h"
#elif defined MSVC2012
#	include "msvc2012.h"
#elif defined MSVC2005
#	include "msvc2005.h"
#elif defined VCRT64
#	include "vcrt64.h"
#endif
