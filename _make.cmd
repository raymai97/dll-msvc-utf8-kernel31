@echo off
setlocal enableDelayedExpansion
if defined 64bit (
call "D:\vcrt64\vcvar64.cmd" || goto badEnv
set myDefine=-DKERNEL31_EXPORTS -DKERNEL31_64BIT
) else (
call "D:\vc60\vcvar.cmd" || goto badEnv
set myDefine=-DKERNEL31_EXPORTS
)
if not "%~1"=="" (
set myDefine=-D%~1 !myDefine!
)
pushd %~dp0

cl -nologo ^
	-LD -O2 -EHsc ^
	!myDefine! ^
	kernel31.cpp ^
	-link ^
	-def:kernel31.def ^
	-out:kernel31.dll ^
	-entry:DllMain ^
	-noDefaultLib ^
	-opt:nowin98 ^
	kernel32.lib ^
	user32.lib || goto halt
del *.exp;*.lib;*.obj
popd
exit/b

:badEnv
echo MSVC environment not ready!
pause
exit/b

:halt
popd
pause
exit/b
