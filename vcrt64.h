#pragma comment(linker, "/export:CloseHandle=kernel32.CloseHandle")
#pragma comment(linker, "/export:CreateEventW=kernel32.CreateEventW")
#pragma comment(linker, "/export:CreateFileMappingW=kernel32.CreateFileMappingW")
#pragma comment(linker, "/export:CreateFileW=kernel32.CreateFileW")
#pragma comment(linker, "/export:CreateMutexW=kernel32.CreateMutexW")
#pragma comment(linker, "/export:CreatePipe=kernel32.CreatePipe")
#pragma comment(linker, "/export:CreateProcessA=kernel32.CreateProcessA")
#pragma comment(linker, "/export:CreateProcessW=kernel32.CreateProcessW")
#pragma comment(linker, "/export:CreateSemaphoreW=kernel32.CreateSemaphoreW")
#pragma comment(linker, "/export:CreateThread=kernel32.CreateThread")
#pragma comment(linker, "/export:DeleteCriticalSection=kernel32.DeleteCriticalSection")
#pragma comment(linker, "/export:DisableThreadLibraryCalls=kernel32.DisableThreadLibraryCalls")
#pragma comment(linker, "/export:DuplicateHandle=kernel32.DuplicateHandle")
#pragma comment(linker, "/export:EnterCriticalSection=kernel32.EnterCriticalSection")
#pragma comment(linker, "/export:ExitProcess=kernel32.ExitProcess")
#pragma comment(linker, "/export:FindClose=kernel32.FindClose")
#pragma comment(linker, "/export:FindFirstFileW=kernel32.FindFirstFileW")
#pragma comment(linker, "/export:FindNextFileW=kernel32.FindNextFileW")
#pragma comment(linker, "/export:FindResourceW=kernel32.FindResourceW")
#pragma comment(linker, "/export:FormatMessageW=kernel32.FormatMessageW")
#pragma comment(linker, "/export:FreeLibrary=kernel32.FreeLibrary")
#pragma comment(linker, "/export:GetACP=kernel32.GetACP")
#pragma comment(linker, "/export:GetCPInfo=kernel32.GetCPInfo")
#pragma comment(linker, "/export:GetCommandLineW=kernel32.GetCommandLineW")
#pragma comment(linker, "/export:GetComputerNameW=kernel32.GetComputerNameW")
#pragma comment(linker, "/export:GetConsoleMode=kernel32.GetConsoleMode")
#pragma comment(linker, "/export:GetConsoleOutputCP=kernel32.GetConsoleOutputCP")
#pragma comment(linker, "/export:GetConsoleScreenBufferInfo=kernel32.GetConsoleScreenBufferInfo")
#pragma comment(linker, "/export:GetCurrentDirectoryW=kernel32.GetCurrentDirectoryW")
#pragma comment(linker, "/export:GetCurrentProcess=kernel32.GetCurrentProcess")
#pragma comment(linker, "/export:GetCurrentProcessId=kernel32.GetCurrentProcessId")
#pragma comment(linker, "/export:GetCurrentThreadId=kernel32.GetCurrentThreadId")
#pragma comment(linker, "/export:GetDiskFreeSpaceExW=kernel32.GetDiskFreeSpaceExW")
#pragma comment(linker, "/export:GetEnvironmentStringsW=kernel32.GetEnvironmentStringsW")
#pragma comment(linker, "/export:GetEnvironmentVariableW=kernel32.GetEnvironmentVariableW")
#pragma comment(linker, "/export:GetFileAttributesExW=kernel32.GetFileAttributesExW")
#pragma comment(linker, "/export:GetFileAttributesW=kernel32.GetFileAttributesW")
#pragma comment(linker, "/export:GetFileSize=kernel32.GetFileSize")
#pragma comment(linker, "/export:GetFileTime=kernel32.GetFileTime")
#pragma comment(linker, "/export:GetFileType=kernel32.GetFileType")
#pragma comment(linker, "/export:GetFullPathNameW=kernel32.GetFullPathNameW")
#pragma comment(linker, "/export:GetLastError=kernel32.GetLastError")
#pragma comment(linker, "/export:GetLocaleInfoA=kernel32.GetLocaleInfoA")
#pragma comment(linker, "/export:GetLongPathNameW=kernel32.GetLongPathNameW")
#pragma comment(linker, "/export:GetModuleFileNameW=kernel32.GetModuleFileNameW")
#pragma comment(linker, "/export:GetModuleHandleW=kernel32.GetModuleHandleW")
#pragma comment(linker, "/export:GetProcAddress=kernel32.GetProcAddress")
#pragma comment(linker, "/export:GetShortPathNameW=kernel32.GetShortPathNameW")
#pragma comment(linker, "/export:GetStartupInfoW=kernel32.GetStartupInfoW")
#pragma comment(linker, "/export:GetStdHandle=kernel32.GetStdHandle")
#pragma comment(linker, "/export:GetSystemInfo=kernel32.GetSystemInfo")
#pragma comment(linker, "/export:GetSystemTimeAsFileTime=kernel32.GetSystemTimeAsFileTime")
#pragma comment(linker, "/export:GetTempFileNameW=kernel32.GetTempFileNameW")
#pragma comment(linker, "/export:GetTempPathW=kernel32.GetTempPathW")
#pragma comment(linker, "/export:GetThreadLocale=kernel32.GetThreadLocale")
#pragma comment(linker, "/export:GetTickCount=kernel32.GetTickCount")
#pragma comment(linker, "/export:GetUserDefaultUILanguage=kernel32.GetUserDefaultUILanguage")
#pragma comment(linker, "/export:GetVersionExA=kernel32.GetVersionExA")
#pragma comment(linker, "/export:GetVersionExW=kernel32.GetVersionExW")
#pragma comment(linker, "/export:InitializeCriticalSection=kernel32.InitializeCriticalSection")
#pragma comment(linker, "/export:InterlockedDecrement=kernel32.InterlockedDecrement")
#pragma comment(linker, "/export:InterlockedExchange=kernel32.InterlockedExchange")
#pragma comment(linker, "/export:InterlockedIncrement=kernel32.InterlockedIncrement")
#pragma comment(linker, "/export:LCMapStringW=kernel32.LCMapStringW")
#pragma comment(linker, "/export:LeaveCriticalSection=kernel32.LeaveCriticalSection")
#pragma comment(linker, "/export:LoadLibraryA=kernel32.LoadLibraryA")
#pragma comment(linker, "/export:LoadLibraryExW=kernel32.LoadLibraryExW")
#pragma comment(linker, "/export:LoadLibraryW=kernel32.LoadLibraryW")
#pragma comment(linker, "/export:LoadResource=kernel32.LoadResource")
#pragma comment(linker, "/export:LocalAlloc=kernel32.LocalAlloc")
#pragma comment(linker, "/export:MapViewOfFile=kernel32.MapViewOfFile")
#pragma comment(linker, "/export:MapViewOfFileEx=kernel32.MapViewOfFileEx")
//#pragma comment(linker, "/export:MultiByteToWideChar=kernel32.MultiByteToWideChar")
#pragma comment(linker, "/export:MultiByteToWideChar=kernel31.My_MultiByteToWideChar")
#pragma comment(linker, "/export:QueryPerformanceCounter=kernel32.QueryPerformanceCounter")
#pragma comment(linker, "/export:RaiseException=kernel32.RaiseException")
#pragma comment(linker, "/export:ReadFile=kernel32.ReadFile")
#pragma comment(linker, "/export:ReleaseMutex=kernel32.ReleaseMutex")
#pragma comment(linker, "/export:ReleaseSemaphore=kernel32.ReleaseSemaphore")
#pragma comment(linker, "/export:SetConsoleCtrlHandler=kernel32.SetConsoleCtrlHandler")
#pragma comment(linker, "/export:SetEnvironmentVariableW=kernel32.SetEnvironmentVariableW")
#pragma comment(linker, "/export:SetEvent=kernel32.SetEvent")
#pragma comment(linker, "/export:SetFilePointer=kernel32.SetFilePointer")
#pragma comment(linker, "/export:SetFileTime=kernel32.SetFileTime")
#pragma comment(linker, "/export:SetLastError=kernel32.SetLastError")
#pragma comment(linker, "/export:SetThreadPriority=kernel32.SetThreadPriority")
#pragma comment(linker, "/export:SizeofResource=kernel32.SizeofResource")
#pragma comment(linker, "/export:Sleep=kernel32.Sleep")
#pragma comment(linker, "/export:UnmapViewOfFile=kernel32.UnmapViewOfFile")
#pragma comment(linker, "/export:VirtualAlloc=kernel32.VirtualAlloc")
#pragma comment(linker, "/export:VirtualFree=kernel32.VirtualFree")
#pragma comment(linker, "/export:VirtualQuery=kernel32.VirtualQuery")
#pragma comment(linker, "/export:WaitForMultipleObjects=kernel32.WaitForMultipleObjects")
#pragma comment(linker, "/export:WaitForSingleObject=kernel32.WaitForSingleObject")
//#pragma comment(linker, "/export:WideCharToMultiByte=kernel32.WideCharToMultiByte")
#pragma comment(linker, "/export:WideCharToMultiByte=kernel31.My_WideCharToMultiByte")
#pragma comment(linker, "/export:WriteFile=kernel32.WriteFile")
#pragma comment(linker, "/export:lstrcmpiW=kernel32.lstrcmpiW")
#pragma comment(linker, "/export:lstrcpyW=kernel32.lstrcpyW")
#pragma comment(linker, "/export:lstrcpynW=kernel32.lstrcpynW")
#pragma comment(linker, "/export:lstrlenA=kernel32.lstrlenA")
#pragma comment(linker, "/export:lstrlenW=kernel32.lstrlenW")
