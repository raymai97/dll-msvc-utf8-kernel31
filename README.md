# kernel31 for UTF-8 in MSVC

If your system locale is not English, and your source files don't have BOM, MSVC will treat your source file in some non-Unicode encoding. [(More info here)](https://raymai97.github.io/myblog/msvc-support-utf8-string-literal-since-vc6) This patch will force MSVC to treat your file as UTF-8 by lying to the compiler that your system locale is English.

For now, you need to mod the related files (`cl.exe`, `c1.dll`, `c1xx.dll`, `c2.dll`) manually. To mod a file, open the file in hex editor (such as HxD) and replace the string `kernel32` to `kernel31` so our `kernel31.dll` can alter the API call before passing to the real `kernel32.dll`.
